# Botocryn

Little IRC bot providing different functionalities.

My first Crystal project. To get the best learning effect,
i only used the resource from the Crystal Website(language reference
and standard library api docs) and rfc1459 for help.

## Installation

`shards build src/botocryn.cr`

## Usage

First set the network/channel to be joined in src/botocryn.cr, then
(re)compile and run.

When the Bot joined your channel type `!help` to get the commands.

## Development

TODO: Write development instructions here

## Contributing

1. Fork it (<https://codeberg.org/repo/fork/27639>)
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create a new Pull Request

## Contributors

- [Elouin](https://codeberg.org/Elouin) - creator and maintainer
