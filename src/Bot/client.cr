require "log"
require "../IRC/*"

module Bot
  # holds the connection, automatically replies to PING etc
  class Client
    getter conn : IRC::Conn
    getter channels : Array(String)

    def initialize(hostname : String, port : Int16, ssl : Bool, nick : String, channels : Array(String))
      @conn = IRC::Conn.new(hostname, port, ssl, nick)
      @channels = channels
      self.send IRC::Message.nick(nick)
      Log.info { "send nick" }
      self.send IRC::Message.user(nick)
      Log.info { "send user" }

      loop do
        message = self.rcv
        if message.command == "221" || message.command == "MODE"
          @channels.each do |channel|
            self.send IRC::Message.join(channel)
            Log.info { "send join request for channel #{channel}" }
          end
          break
        end
      end
    end

    def rcv()
      loop do
        raw_message = @conn.gets
        if raw_message.is_a?(String)
          message = IRC::Message.new(raw_message)
        else
          next
        end
        Log.info { "recieved Message: #{raw_message}" }
        if message.command == "PING"
          self.send message.ping
          Log.info { "send ping response" }
        elsif message.command == "JOIN" && message.sender_nick == @conn.nick
          Log.info { "joined channel #{message.content}" }
        else
          return message
        end
      end
    end

    def start()
      loop do
        message = self.rcv
        spawn do
          self.message_handler(message)
        end
      end
    end

    def message_handler(message : IRC::Message)
      if message.command == "PRIVMSG"
        if message.content.includes?("!info")
          self.send IRC::Message.privmsg(channels[0], "I am a botocryn IRC bot. https://codeberg.org/Elouin/Botocryn")
        elsif message.content.includes?("!ping")
          self.send IRC::Message.privmsg(channels[0], "pong #{message.sender_nick}")
        elsif message.content.includes?("!help")
          self.send IRC::Message.privmsg(channels[0], "Commands: !info, !help, !ping")
        elsif url = message.content.match(/http[s]?:\/\/(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+/)
          res = HTTP::Client.get(url[0])
          body = XML.parse_html(res.body)
          if title = body.xpath_node("/html/head/title")
            title.content.strip
            self.send IRC::Message.privmsg(channels[0], "#{title.content.strip}")
          end
        end
      end
    end

    def send(message)
      conn.puts(message)
    end
  end
end
