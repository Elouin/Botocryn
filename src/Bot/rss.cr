require "log"
require "xml"
require "uri"

module Bot
  class RSS_Reader
    getter client : Bot::Client
    getter url : String | URI
    getter timeout : Int16

    def initialize(client : Bot::Client, url : String | URI, timeout : Int16)
      @client = client
      @url = url
      @timeout = timeout
    end

    def read_feed(url : String) : XML::Node
      res = HTTP::Client.get(url[0])
      body = XML.parse(res.body)
    end
  end
end
