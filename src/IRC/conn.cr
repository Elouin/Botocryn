require "socket"
require "openssl"

module IRC
  # Connection to the IRC Server
  class Conn
    getter host : String
    getter port : Int16
    getter ssl : Bool
    getter nick : String
    getter real_name : String
    
    # Initializes connection to a IRC server.
    #
    # Requires at least a *host* and a *port*.
    def initialize(host : String, port : Int16, ssl : Bool = false, nick : String = "Botocryn", real_name : String = "Boto Cryn")
      @host = host
      @port = port
      @ssl = ssl
      @nick = nick
      @real_name = real_name
      if @ssl
        @serv_conn = OpenSSL::SSL::Socket::Client.new(TCPSocket.new(@host, @port))
      else
        @serv_conn = TCPSocket.new(@host, @port)
      end
    end

    # Gets the next message from the server.
    def gets
      @serv_conn.gets
    end

    # Sends a message to the server.
    def puts(message : String)
      @serv_conn.puts message
      @serv_conn.flush
    end
  end
end
