module IRC
  # Represents an IRC message
  class Message
    getter sender
    getter command
    getter target
    getter content

    # expects *message* to be a raw irc message
    def initialize(message : String)
      @sender = ""
      @command = ""
      @target = ""
      @content = ""
      parse message
    end

    # parsing messages from server
    def parse(message : String)
      if message.starts_with?(':')
        sections : Array(String) = message.split
        if sections.size < 4
          return
        end
        @sender = sections[0]
        @command = sections[1]
        @target = sections[2]
        @content = sections[3..sections.size-1].join(' ')
      elsif
        sections = message.split
        @sender = ""
        @command = sections[0]
        @target = ""
        @content = sections[1..sections.size-1].join(' ')
      end
    end

    # returns the nick of the sender
    def sender_nick
      @sender.split('!')[0][1..-1]
    end

    # answer ping messages from server
    def ping(content : String = @content)
      "PONG #{content}"
    end

    def self.privmsg(target : String, content : String)
      "PRIVMSG #{target} :#{content}"
    end

    # generates join channel message
    def self.join(channel : String = @target)
      "JOIN #{channel}"
    end
    
    def self.nick(nick : String)
      "NICK #{nick}"
    end

    def self.user(nick : String, real_name : String = "Botocryn IRC Bot written in Crystal")
      "USER #{nick} 0 * :#{real_name}"
    end

    def self.quit(message : String = @content)
      "QUIT #{message}"
    end
  end
end
