require "socket"
require "openssl"
require "log"
require "http/client"
require "xml"
require "ini"
require "file_utils"

require "./IRC/*"
require "./Bot/*"

CONF_PATHS = [FileUtils.pwd, "#{Path.home}/.config"]
conf : Hash(String, Hash(String, String)) = {} of String => Hash(String, String)

CONF_PATHS.each do |path|
  begin
    conf = INI.parse(File.open("#{path}/botocryn.conf"))
  rescue File::NotFoundError
    next
  end
end

if conf == {} of String => Hash(String, String)
  conf["connection"] = {} of String => String
  conf["channels"] = {} of String => String
  puts "No config file found."
  puts "network to connect to: "
  conf["connection"]["network"] = gets.to_s
  puts "Port: "
  conf["connection"]["port"] = gets.to_s
  puts "SSL: "
  conf["connection"]["ssl"] = gets.to_s
  puts "Nick for the Bot: "
  conf["connection"]["nick"] = gets.to_s
  puts "Channel to join: "
  conf["channels"]["channel1"] = gets.to_s
end

File.write("#{Path.home}/.config/botocryn.conf", INI.build(conf))

ssl : Bool = conf["connection"]["ssl"] == "true"
hostname : String = conf["connection"]["network"]
port : Int16 = conf["connection"]["port"].to_i16
nick : String = conf["connection"]["nick"]
channels : Array(String) = [] of String
conf["channels"].each do |channel|
  channels << channel[1]
end

Bot::Client.new(hostname, port, ssl, nick, channels).start

